#!/usr/bin/env python3
from time import sleep
from bothelper import read_board, up, down, left, right, botsetup, rotate_board, merge_count, print_board, is_board_locked, reset
from collections import deque, namedtuple 
import math
import random
from dqnlearner_class import DQNLearner
from engine import Engine
from itertools import count
import numpy as np
import sys

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

setup = botsetup("Glicko")

BATCH_SIZE = 16
GAMMA = 0.99
EPS_START = 0.9
EPS_END = 0.001
EPS_DECAY = 1000
TAU = 0.005
LR = 1e-4

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

actions = [up, right, down, left]
n_actions = len(actions)
n_states = 16

policy_net = DQNLearner(n_states, n_actions).to(device)
target_net = DQNLearner(n_states, n_actions).to(device)
target_net.load_state_dict(policy_net.state_dict())

optimizer = optim.AdamW(policy_net.parameters(), lr=LR, amsgrad=True)

class ReplayMemory(object):

    def __init__(self, capacity):
        self.memory = deque([], maxlen=capacity)

    def push(self, *args):
        """Save a transition"""
        self.memory.append(Transition(*args))

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)
    
memory = ReplayMemory(10000)

steps_done = 0

def select_action(state):
    global steps_done
    sample = random.random()
    eps_threshold = EPS_END + (EPS_START - EPS_END) * \
        math.exp(-1. * steps_done / EPS_DECAY)
    steps_done += 1
    if sample > eps_threshold:
        with torch.no_grad():
            # t.max(1) will return the largest column value of each row.
            # second column on max result is index of where max element was
            # found, so we pick action with the larger expected reward.
            return policy_net(state).max(1).indices.view(1, 1)
    else:
        return torch.tensor([[random.randint(0, 3)]], device=device, dtype=torch.long)


def optimize_model():
    transitions = memory.sample(BATCH_SIZE if BATCH_SIZE < len(memory) else len(memory))
    # Transpose the batch (see https://stackoverflow.com/a/19343/3343043 for
    # detailed explanation). This converts batch-array of Transitions
    # to Transition of batch-arrays.
    batch = Transition(*zip(*transitions))

    # Compute a mask of non-final states and concatenate the batch elements
    # (a final state would've been the one after which simulation ended)
    non_final_mask = torch.tensor(tuple(map(lambda s: s is not None,
                                          batch.next_state)), device=device, dtype=torch.bool)
    
    non_final_next_states = torch.cat([s for s in batch.next_state
                                                if s is not None])
    state_batch = torch.cat(batch.state)
    action_batch = torch.cat(batch.action)
    reward_batch = torch.cat(batch.reward)

    state_action_values = policy_net.forward(state_batch).gather(1, action_batch)

    next_state_values = torch.zeros(BATCH_SIZE if BATCH_SIZE < len(memory) else len(memory), device=device)
    with torch.no_grad():
        next_state_values[non_final_mask] = target_net(non_final_next_states).max(1).values
    # Compute the expected Q values
    expected_state_action_values = (next_state_values * GAMMA) + reward_batch

    # Compute Huber loss
    criterion = nn.SmoothL1Loss()
    loss = criterion(state_action_values, expected_state_action_values.unsqueeze(1))

    # Optimize the model
    optimizer.zero_grad()
    loss.backward()
    # In-place gradient clipping
    torch.nn.utils.clip_grad_value_(policy_net.parameters(), 100)
    optimizer.step()

    

################### MAIN
    
episode_info = []

Transition = namedtuple('Transition', ('state', 'action', 'next_state', 'reward'))


if torch.cuda.is_available():
    num_episodes = 600
else:
    num_episodes = 50

for i_episode in range(num_episodes):

    # Initialize the environment and get its state
    state, score = read_board(setup, True)
    print(state)
    state = np.array(state).astype(int).flatten()
    state = torch.tensor(state, dtype=torch.float, device=device).unsqueeze(0)

    for t in count():
        # Select next action
        action = select_action(state) 

        # Execute action
        actions[action[0, 0].item()](setup)

        # Read new board
        observation, score = read_board(setup, True)
        
        # Check reward
        reward, s = merge_count(observation)
        reward = torch.tensor([reward], device=device)

        # Check if game is finished
        done = is_board_locked(observation)

        if done:
            next_state = None
        else:
            # Redefine next state
            observation_flatten = np.array(observation).astype(int).flatten()
            next_state = torch.tensor(observation_flatten, dtype=torch.float32, device=device).unsqueeze(0)

        # Memory push
        memory.push(state, action, next_state, reward)

        # Move to the next state
        state = next_state

        # Perform one step of the optimization (on the policy network)
        optimize_model()

        # Soft update of the target network's weights
        # θ′ ← τ θ + (1 −τ )θ′
        target_net_state_dict = target_net.state_dict()
        policy_net_state_dict = policy_net.state_dict()
        for key in policy_net_state_dict:
            target_net_state_dict[key] = policy_net_state_dict[key]*TAU + target_net_state_dict[key]*(1-TAU)
        target_net.load_state_dict(target_net_state_dict)

        if done:
            episode_info.append([score, t + 1])
            break
    
    # When episode ends
    # if i_episode%20 == 0 : 
    print(f"Episode {i_episode + 1} : ", score, t+1)
    reset(setup)
    sleep(0.1)

print('Complete')

sorted_info_episodes = sorted(episode_info, reverse=True, key=lambda x: x[0])
best_info_episodes = sorted_info_episodes[:5]

print("Voici les 5 meilleurs scores : ")
for score, coup in best_info_episodes:
    print(f"Score : {score}, Coup : {coup}")
