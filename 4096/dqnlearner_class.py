import math
import random
import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F


class DQNLearner(nn.Module):

    def __init__(self, n_observations, n_actions):
        super(DQNLearner, self).__init__()
        self.num_states = n_observations
        self.num_actions = n_actions
        self.P = np.zeros((self.num_states, self.num_actions, self.num_states))
        self.R = np.zeros((self.num_states, self.num_actions))
        self.pi = np.zeros(self.num_states, dtype=np.int32)

        self.layer1 = nn.Linear(self.num_states, 128)
        self.layer2 = nn.Linear(128, 128)
        ##self.layer3 = nn.Linear(512, 128)
        self.layer4 = nn.Linear(128, self.num_actions)

    def forward(self, x):
        x = F.relu(self.layer1(x))
        x = F.relu(self.layer2(x))
        ##x = F.relu(self.layer3(x))
        return self.layer4(x)
